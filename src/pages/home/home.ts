import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  imageURI:any;
  imageFileName:any;
  fileTransfer: FileTransferObject;
  constructor(public navCtrl: NavController,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController) {

      this.fileTransfer = this.transfer.create();

    }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }

  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    let options: FileUploadOptions = {
      fileKey: 'avatar',
      fileName: 'avatar.jpeg',
      headers: {'Authorization':'Bearer eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX0NMSUVOVCIsIlJPTEVfVVNFUiJdLCJ1c2VybmFtZSI6InRlc3RAZ21haWwuY29tIiwiZXhwIjo1NTU1NTU1NTU3MDk1NDE3MTMyLCJpYXQiOjE1Mzk4NjE1Nzd9.p4oGZ22YuS_MdxcxxcFhBOeSzXPMuj3g_3yXZ9LlUqAp6G2gSurbkiHZC9x1c4s_n4WnMJHdwscMEU2lA5a90mKtUsRmR1HP_l97wprvxvug8L3GowlBToS7q7F212sSs98b6zAlzwWwWmIpgUfWJHgAeoVlF_6jDJtJPIhFtWrOSXEzvbc2iEq9oXPyX1miP7Kc6eRceQYK8SrSmx5mn7ZZg5W0K3HPOOrqc9kr4tAvLwXwQWkTyaVeu-E5spnUEGZRxvCsx0VUpml6ErGrMhqO_4NVXU8lO1nZeb7a7yFzi8r0uVOu3qIXg2ffOLQfEHS6qfzDHGhru6VAMFi9kRaBqjGLSGa2Ayd9wVEwuomszuEpLEKpXg97RyqK1A0CBZHAL_787S-tUpb8jpHsI_c-oB1x6i0nt5nGKWiQbroQ0jwBvUdN6y5ZngXebtBZoZeMn8GCZguLPM_je5cjES3AzhhtdjkKYWgrPK0gwyJEOzFWiQIBwDpntQksWHDiCVU9DFaYrQsXZrVTkz6d3erSFPNIbmCYA6sjHhXuelCf5VaHxvtlhgg9xwlttqL4H6NePIohVBWyHi7XIsoxCeedgy8qHNQlB1JyUL8jIxdHNqje3o3UglzDqddoeeNlq3XPwQ0rWYRuUwOwRntALWh3T-UCWwl-XY0TshguiF4'}
    }
     alert(JSON.stringify(this.fileTransfer));
    this.fileTransfer.upload(this.imageURI, 'http://51.254.114.105/takouira/web/app_dev.php/api/secure/users/avatars', options)
      .then((data) => {
      console.log(data+" Uploaded Successfully");
     // this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
      loader.dismiss();
      this.presentToast("Image uploaded successfully");
    }, (err) => {
      console.log(err);
      loader.dismiss();
      this.presentToast(err);
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 6000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
